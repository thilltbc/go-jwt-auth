package ezjwtauth

import (
	"github.com/gin-gonic/gin"
	"log"
	"strings"
)

func GinAuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		authHeader := c.GetHeader("Authorization")
		if len(authHeader) < 1 {
			c.AbortWithStatus(401)
			log.Printf("No Auth Header!\n")
			return
		}
		splitAuth := strings.Split(authHeader, " ")
		if len(splitAuth) != 2 {
			log.Printf("Unable to split auth header!\n")
			c.AbortWithStatus(401)
			return
		}
		tokenString := splitAuth[1]
		claims, err := VerifyIDToken(tokenString)
		if err != nil {
			log.Printf("%e\n", err)
			c.AbortWithStatus(401)
			return
		}
		c.Set("claims", claims)
		c.Next()
	}
}
