# Ez JWT Auth

Like Golang? Like Okta? This package helps make using Okta with Golang much easier (at least for me).
**Currently only verification of RS256 JWTs is supported.**
This is my first Golang Package, so helpful issues/PRs are welcome.

## Todos
* Improve logging
* Give other frameworks middleware options
* Compatibility with other Auth providers
* Compability in general...
* Probably a lot of other things

## Thanks
I wrote this package because I was having a hard time figuring out how to parse/verify RS256 JWTs from Okta.
Finding [go-firebase-verify](https://github.com/dkoshkin/go-firebase-verify) helped me tremendously.
Also makes use of [github.com/dgrijalva/jwt-go]("https://github.com/dgrijalva/jwt-go")

## Setup/Usage/Gin Example
Setup is simple, you only need to set your JWKS url; Which for Okta users is just your /keys endpoint.
Then just use the GinAuthMiddleware()
```
package main

import (
	"gitlab.com/thilltbc/go-jwt-auth"
	"github.com/gin-gonic/gin"
)

func main() {
	g := gin.Default()
	//Replace %YOUROKTAHERE% with your own Okta subdomain, they're free.
	ezjwtauth.SetClientCertURL("https://%YOUROKTAHERE%.oktapreview.com/oauth2/default/v1/keys")
	g.Use(ezjwtauth.GinAuthMiddleware())
	g.GET("/", func(context *gin.Context) {
		claims,_ := context.Get("claims")
		context.JSON(200, claims)
	})
	g.Run("127.0.0.1:8000")
}
```